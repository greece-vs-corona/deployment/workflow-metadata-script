import datetime
import logging
import os
import json
import sys
from os import listdir

import pymongo

referral_num = os.getenv('REFERRAL_NUM')
host = os.getenv('HOST')
port = int(os.getenv('PORT'))
username = os.getenv('MONGO_USER')
password = os.getenv('MONGO_PASS')
authSource = os.getenv('AUTHENTICATION_SOURCE')
database = os.getenv('DATABASE')
collection = os.getenv('COLLECTION')

mode = sys.argv[1]

with pymongo.MongoClient(host=host, port=port, username=username, password=password, authSource=authSource) as client:
    db = client[database]
    collection = db[collection]

    query = {"referral_num": referral_num}

    if mode == 'submit':

        value = {"$set": {"status": "Running"}}

    elif mode == 'inputs':

        group = os.getenv('GROUP')

        prefix = 's3://' + group + '/workflows/' + referral_num + '/inputs/'

        inputs = [prefix + file for file in listdir("/mnt/uploads/" + referral_num + "/")]

        value = {"$set": {"inputs": inputs}}

    elif mode == 'outputs':

        group = os.getenv('GROUP')

        prefix = 's3://' + group + '/workflows/' + referral_num + '/outputs/'

        outputs = [prefix + file for file in listdir('/mnt/shared/outputs/')]

        value = {"$set": {"outputs": outputs}}

    elif mode == 'onExit':

        status = sys.argv[2]
        failures = sys.argv[3]
        duration = sys.argv[4]

        vals = {"status": status, 'argo_duration': duration, 'finished_at': datetime.datetime.utcnow()}

        # if no failures argo sets failures='null' else provides a json list
        if failures != 'null':
            vals['failures'] = json.loads(failures)

        value = {"$set": vals}

    result = collection.update_one(query, value)
    
    if result.matched_count > 0:
        logging.info('updated metadata for referral_num: ' + referral_num)
        sys.exit(0)
    else:
        logging.error('nothing to update for referral_num: ' + referral_num + ', check if "referral_num: ' +
                      referral_num + '" exists in the database')
        sys.exit(1)
