FROM python:3.10.1-alpine3.15

RUN pip install pymongo==4.0.1

WORKDIR /app

COPY app.py ./
